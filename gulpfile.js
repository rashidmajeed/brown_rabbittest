var gulp =  require('gulp');
var stylus = require('gulp-stylus');

gulp.task('styles', function () {
    gulp.src('src/css/style.styl')
        .pipe(stylus())
        .pipe(gulp.dest('./src/css'))
    
});

gulp.task('watch:styles', function () {
    gulp.watch('**/*.styl', ['styles']);
});